package ru.artofmainstreams.mappoints;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import ru.artofmainstreams.mappointslibrary.MapPoints;
import ru.artofmainstreams.mappointslibrary.MyLocation;

import static ru.artofmainstreams.mappoints.R.id.map;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private MapPoints mapPoints;
    private MyLocation location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(map);
        mapFragment.getMapAsync(this);
        mapPoints = new MapPoints(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        location = new MyLocation(mapPoints);
        location.setUpLocationListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        location.removeUpdate();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapPoints.setCoords(googleMap);
    }
}
