package ru.artofmainstreams.mappointslibrary;

import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by artofmainstreams on 12.06.2017.
 */

public class MapPoints  {
    private Context context;
    private List<LatLng> coords;
    private final String TAG = "myLogs";

    public MapPoints(Context context) {
        coords = new LinkedList<>();
        this.context = context;
    }

    public void checkLocation(Location location) {
        if (location == null)
            return;

        Log.d(TAG, "My coordinats: " + location.getLatitude() +","
                + location.getLongitude());

        for (LatLng coord:coords) {
            Location locationCoord = new Location("");
            locationCoord.setLatitude(coord.latitude);
            locationCoord.setLongitude(coord.longitude);
            Log.d(TAG, "distance: " + location.distanceTo(locationCoord));

            if (location.distanceTo(locationCoord) <= 100.0) {
                Toast.makeText(context, "Вы у цели", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void setCoords(final GoogleMap mMap) {
        LatLng moscow = new LatLng(55, 37);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(moscow));

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Log.d(TAG, "onMapClick: " + latLng.latitude + "," + latLng.longitude);
                mMap.addMarker(new MarkerOptions().position(latLng).title("Marked"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                coords.add(latLng);
            }
        });
    }
}
