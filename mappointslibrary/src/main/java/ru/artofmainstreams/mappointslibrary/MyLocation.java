package ru.artofmainstreams.mappointslibrary;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

/**
 * Created by artofmainstreams on 13.06.2017.
 */

public class MyLocation implements LocationListener {
    private static final long MIN_TIME_TO_UPDATE_SEC = 1000 * 5;
    private static final float MIN_DISTANCE_TO_UPDATE_METER = 1;
    private LocationManager locationManager;
    private MapPoints mapPoints;

    public MyLocation(MapPoints mapPoints) {
        super();
        this.mapPoints = mapPoints;
    }

    public void setUpLocationListener(Context context) {
        if (ActivityCompat.checkSelfPermission(
                context, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(
                        context, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context, "Не хватает прав, проверьте настройки приложения",
                    Toast.LENGTH_LONG).show();
            return;
        }
        locationManager = (LocationManager)
                context.getSystemService(Context.LOCATION_SERVICE);

        LocationListener locationListener = new MyLocation(mapPoints);

        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                MIN_TIME_TO_UPDATE_SEC,
                MIN_DISTANCE_TO_UPDATE_METER,
                locationListener);
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                MIN_TIME_TO_UPDATE_SEC,
                MIN_DISTANCE_TO_UPDATE_METER,
                locationListener);
    }

    @Override
    public void onLocationChanged(Location location) {
        mapPoints.checkLocation(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        //  изменился статус указанного провайдера
    }

    @Override
    public void onProviderEnabled(String provider) {
        // указанный провайдер был включен пользователем
    }

    @Override
    public void onProviderDisabled(String provider) {
        // указанный провайдер был отключен пользователем
    }

    public void removeUpdate() {
        locationManager.removeUpdates(this);
    }
}
